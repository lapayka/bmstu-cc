﻿#include "polish.h"

#include <iostream>
#include <cassert>
#include <memory>
#include <ranges>
#include <unordered_set>

#include <boost/format.hpp>

struct visitor
{
	virtual std::string visit(const Parser::Node& node) = 0;
};

using visitor_ptr = std::shared_ptr<visitor>;

visitor_ptr dispatch_visitor(const std::string& data);

struct term_visitor;
struct simple_expression_vistor : public visitor
{
	virtual std::string visit(const Parser::Node& node) override;
};

struct factor_visitor : public visitor
{
	virtual std::string visit(const Parser::Node& node) override
	{
		if (node.children.size() == 1)
		{
			if (node.children.front().data.starts_with("Factor"))
			{
				return factor_visitor().visit(node.children.front()) + "(not)";
			}
			else
			{
				return node.children.front().children.front().data;
			}
		}
		else if (node.children.size() == 3)
		{
			return simple_expression_vistor().visit(node.children[1]);
		}
	}
};

struct term_visitor : public visitor
{
	virtual std::string visit(const Parser::Node& node) override
	{
		if (node.children.front().data.starts_with("eps"))
		{
			return {};
		}
		else if (node.children.front().data.starts_with("Factor"))
		{
			std::string s2 = factor_visitor().visit(node.children.front());
			std::string s1 = term_visitor().visit(node.children.back());

			return (boost::format("(%s)(%s)") % s1 % s2).str();
		}
		else if (node.children.front().data.starts_with("MultiplicationOperation"))
		{
			std::string s3 = node.children[0].children[0].data;
			std::string s2 = factor_visitor().visit(node.children[1]);
			std::string s1 = term_visitor().visit(node.children[2]);

			return (boost::format("(%s)(%s)(%s)") % s1 % s2 % s3).str();
		}
		else
		{
			assert(0);
		}
	}
};

std::string simple_expression_vistor::visit(const Parser::Node& node)
{
	if (node.children.front().data.starts_with("eps"))
	{
		return {};
	}
	else if (node.children.front().data.starts_with("Term"))
	{
		std::string s2 = term_visitor().visit(node.children.front());
		std::string s1 = simple_expression_vistor().visit(node.children[1]);
		return (boost::format("(%s)(%s)") % s1 % s2).str();
	}
	else if (node.children.front().data.starts_with("Sign"))
	{
		std::string s3 = node.children[0].children[0].data;
		std::string s2 = term_visitor().visit(node.children[1]);
		std::string s1 = simple_expression_vistor().visit(node.children[2]);

		return (boost::format("(%s)(%s)(%s)") % s1 % s2 % s3).str();
	}
	else if (node.children.front().data.starts_with("AdditionOperation"))
	{
		std::string s3 = node.children[0].children[0].data;
		std::string s2 = term_visitor().visit(node.children[1]);
		std::string s1 = simple_expression_vistor().visit(node.children[2]);

		return (boost::format("(%s)(%s)(%s)") % s1 % s2 % s3).str();
	}
	else
	{
		assert(0);
	}
}

struct expression_vistor : public visitor
{
	virtual std::string visit(const Parser::Node& node) override
	{
		if (node.children.size() == 1)
		{
			return simple_expression_vistor().visit(node.children[0]);
		}
		else if (node.children.size() == 2)
		{
			std::string s3 = node.children[1].children[0].children.front().data;
			std::string s2 = simple_expression_vistor().visit(node.children[0]);
			std::string s1 = simple_expression_vistor().visit(node.children[1].children[1]);

			return (boost::format("(%s)(%s)(%s)") % s1 % s2 % s3).str();
		}
		else
		{
			assert(0);
		}
	}
};


visitor_ptr dispatch_visitor(const std::string& data)
{
	if (data.starts_with("SimpleExpression"))
	{

	}

	return {};
}

using namespace std::string_view_literals;
static std::unordered_set<std::string_view> black_list = { "eps"sv, "("sv, ")"sv }; 

std::string get_polish(const Parser::Node& node)
{
	std::function<std::string(const Parser::Node&)> traverse = [&traverse](const Parser::Node& node) -> std::string
		{
			if (node.children.size() == 0)
			{
				if (black_list.find(node.data) != black_list.end())
					return {};
				else
					return "|" + node.data + "|";
			}

			std::string accum;
			
			size_t size = node.children.size() - (node.children.back().data == "eps");

			if (size >= 3)
			{
				accum += traverse(node.children[0]);
				size_t i = 0;

				while (i + 2 < node.children.size())
				{
					accum += traverse(node.children[i + 2]);
					accum += traverse(node.children[i + 1]);

					i += 2;
				}

				
				//accum += traverse(node.children.back());
			}
			else
			{
				for (const auto sub_node : std::views::reverse(node.children))
				{
					accum += traverse(sub_node);
				}
			}

			return accum;
		};

//	auto ret = expression_vistor().visit(node);
//	std::cout << ret << std::endl;

	return traverse(node);
}