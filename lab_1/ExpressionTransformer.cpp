#include "ExpressionTransformer.h"

#include <cassert>
#include <unordered_map>

#include "MyStack.h"
#include "Operations.h"

std::string ExpressionTransformer::ToPolish(const std::string& regular_expression)
{
    


    my_stack<char> stack;
    std::string res;
    res.reserve(regular_expression.size());
    for (const auto& elem : regular_expression)
    {
        decltype(operations)::iterator iter;
        if ((iter = operations.find(elem)) == operations.end())
            res.push_back(elem);
        else
        {
            if (iter->first == ')')
            {
                
                for (char stack_elem; stack.pop(stack_elem) && stack_elem != '(';)
                    res.push_back(stack_elem);
            }
            else
            {
                if (iter->first != '(' && !stack.empty())
                {
                    for (char stack_elem = stack.top(); operations.find(stack_elem)->second.priority > iter->second.priority;)
                    {
                        stack.pop(stack_elem);
                        res.push_back(stack_elem);
                        if (!stack.empty())
                            stack_elem = stack.top();
                        else
                            break;
                    }
                }

                stack.push(elem);
            }
        }
    }

    for (char stack_elem; stack.pop(stack_elem);)
        res.push_back(stack_elem);

    return res;
}
