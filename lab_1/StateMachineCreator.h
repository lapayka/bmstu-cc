#pragma once

#include <string>

#include "StateMachine.h"

class StateMachineCreator
{
public:
	static StateMachine CreateStateMachineByExpression(const std::string &reg_exp_polish);

private:
	static StateMachine CreateIterationStateMachineTemplate();
	static StateMachine CreatePositiveIterationStateMachineTemplate();
	static StateMachine CreateUnionStateMachineTemplate();
	static StateMachine CreateConcatStateMachineTemplate();

	static StateMachine CreateOneSymbolStateMachine(char symbol);
	static StateMachine CreateIterationStateMachine(const StateMachine& );
	static StateMachine CreatePositiveIterationStateMachine(const StateMachine& );
	static StateMachine CreateUnionStateMachine(const StateMachine &, const StateMachine &);
	static StateMachine CreateConcatStateMachine(const StateMachine&, const StateMachine&);
};

