﻿#include <iostream>
#include <fstream>

#include "ExpressionTransformer.h"
#include "StateMachineCreator.h"

int main()
{
    std::string reg_exp;
    std::string input;

    std::cout << "Input regex\n";
    std::cin >> reg_exp;


    std::cout << ExpressionTransformer::ToPolish(reg_exp) << std::endl;

    auto sm = StateMachineCreator::CreateStateMachineByExpression(ExpressionTransformer::ToPolish(reg_exp));
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 1.svg");
        system("explorer 1.svg");
    }

    sm.DeleteEps();
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 2.svg");
        system("explorer 2.svg");
    }


    //sm.Determine();
    //{
    //    std::ofstream os("out.dot");
    //    sm.ToDot(os);
    //    os.close();
    //    system("dot -Tsvg out.dot -o 3.svg");
    //    system("explorer 3.svg");
    //}

    sm.Reverse();
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 4.svg");
        system("explorer 4.svg");
    }
    sm.Determine();
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 4.svg");
        system("explorer 4.svg");
    }
    sm.Reverse();
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 4.svg");
        system("explorer 4.svg");
    }
    sm.Determine();
    {
        std::ofstream os("out.dot");
        sm.ToDot(os);
        os.close();
        system("dot -Tsvg out.dot -o 4.svg");
        system("explorer 4.svg");
    }

    while (true)
    {
        std::cout << "Input input chain\n";
        std::cin >> input;
        std::cout << (sm.Imitate(input) ? "Match\n" : "Doesn't match\n");
    }
}
