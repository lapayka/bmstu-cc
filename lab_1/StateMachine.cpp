#include "StateMachine.h"

#include <cassert>
#include <ostream>
#include <set>
#include <functional>

StateMachine& StateMachine::InsertStateMachine(StateMachine rhs)
{
	assert(m_unfinished_vertexes.size() >= 1);
	assert(rhs.m_unfinished_vertexes.size() == 0);

	auto vertexes_count = m_graph.size();
	rhs.m_begin_vertex += vertexes_count;
	rhs.m_end_vertex += vertexes_count;
	for (auto &list : rhs.m_graph)
	{
		for (auto &vertex : list)
		{
			vertex += vertexes_count;
		}
	}

	m_graph.insert(m_graph.end(), rhs.m_graph.begin(), rhs.m_graph.end());
	m_rules.insert(m_rules.end(), rhs.m_rules.begin(), rhs.m_rules.end());

	m_graph[m_unfinished_vertexes.rbegin()->first].push_back(rhs.m_begin_vertex);
	m_rules[m_unfinished_vertexes.rbegin()->first].push_back('\0');
	m_graph[rhs.m_end_vertex].push_back(m_unfinished_vertexes.rbegin()->second);
	m_rules[rhs.m_end_vertex].push_back('\0');

	m_unfinished_vertexes.resize(m_unfinished_vertexes.size() - 1);

	return *this;
}

void StateMachine::DeleteEps()
{
	StateMachine _res;

	std::vector<size_t> vertexes_ind;
	{
		std::set<size_t> new_vertexes;

		new_vertexes.insert(m_begin_vertex);
		for (size_t i = 0; i < m_graph.size(); i++)
		{
			for (size_t j = 0; j < m_graph[i].size(); j++)
			{
				if (m_rules[i][j] != '\0')
				{
					new_vertexes.insert(m_graph[i][j]);
				}
			}
		}
		vertexes_ind.assign(new_vertexes.begin(), new_vertexes.end());
	}

	auto index_trasform = [&vertexes_ind](size_t old_idx)
		{
			return std::distance(vertexes_ind.begin(), std::find(vertexes_ind.begin(), vertexes_ind.end(), old_idx));
		};

	for (const auto& _vertex_ind : vertexes_ind)
	{
		using res_type = std::pair<std::vector<size_t>, std::vector<char>>;
		std::function<res_type (size_t)> find_edges = [&](size_t vertex_ind)
			{
				res_type res;
				const auto& edges = m_graph[vertex_ind];

				for (size_t i = 0; i < edges.size(); i++)
				{		
					if (m_rules[vertex_ind][i] != '\0')
					{
						
						res.first.push_back(index_trasform(edges[i]));
						res.second.push_back(m_rules[vertex_ind][i]);
					}
					else
					{
						if (m_graph[vertex_ind][i] == m_end_vertex)
							_res.AddEndVertex(index_trasform(_vertex_ind));

						res_type tmp = find_edges(edges[i]);
						res.first.insert(res.first.end(), tmp.first.begin(), tmp.first.end());
						res.second.insert(res.second.end(), tmp.second.begin(), tmp.second.end());
					}
				}
				return res;
			};

		auto new_edges = find_edges(_vertex_ind);
		_res.m_graph.push_back(new_edges.first);
		_res.m_rules.push_back(new_edges.second);
		_res.m_begin_vertex = index_trasform(m_begin_vertex);
	}
	*this = _res;
}

void StateMachine::Determine()
{
	std::set<char> alphabet;
	for (const auto& row : m_rules)
	{
		for (const auto& elem : row)
		{
			alphabet.insert(elem);
		}
	}

	if (m_begin_vertexes.empty())
		m_begin_vertexes = { m_begin_vertex };

	std::vector<std::set<size_t>> vertexes_unions = { m_begin_vertexes };
	std::vector<std::vector<size_t>> new_graph(1);
	std::vector<std::vector<char>> new_rules(1);

	bool end = false;
	auto vertexes_unions_idx = 0;
	while (vertexes_unions_idx < vertexes_unions.size())
	{
		end = true;

		std::vector<std::set<size_t>> new_vertexes_unions;
		for (auto vertex : vertexes_unions[vertexes_unions_idx])
		{
			for (const auto symbol : alphabet)
			{
				std::set<size_t> new_vertexes_union;

				for (size_t i = 0; i < m_rules[vertex].size(); i++)
					if (m_rules[vertex][i] == symbol)
						new_vertexes_union.insert(m_graph[vertex][i]);

				if (!new_vertexes_union.empty())
				{
					auto iter1 = std::find(vertexes_unions.begin(), vertexes_unions.end(), new_vertexes_union);
					if (iter1 != vertexes_unions.end())
					{
						new_graph[vertexes_unions_idx].push_back(std::distance(vertexes_unions.begin(), iter1));
						new_rules[vertexes_unions_idx].push_back(symbol);
					}
					else
					{
						auto iter2 = std::find(new_vertexes_unions.begin(), new_vertexes_unions.end(), new_vertexes_union);
						new_graph[vertexes_unions_idx].push_back(std::distance(vertexes_unions.begin(), iter1) + std::distance(new_vertexes_unions.begin(), iter2));
						new_rules[vertexes_unions_idx].push_back(symbol);
						if (iter2 == new_vertexes_unions.end())
							new_vertexes_unions.push_back(new_vertexes_union);
						end = false;
					}
				}
			}
		}
		vertexes_unions.insert(vertexes_unions.end(), new_vertexes_unions.begin(), new_vertexes_unions.end());
		new_graph.push_back({});
		new_rules.push_back({});
		++vertexes_unions_idx;
	}

	StateMachine res;
	res.m_begin_vertexes = {0};
	res.m_graph = new_graph;
	res.m_rules = new_rules;
	for (size_t i = 0; i < vertexes_unions.size(); i++)
	{
		for (const auto& vertex : vertexes_unions[i])
		{
			if (m_end_vertexes.find(vertex) != m_end_vertexes.end())
				res.m_end_vertexes.insert(i);
		}
	}
	*this = res;
}

void StateMachine::Reverse()
{
	if (m_begin_vertexes.empty())
		m_begin_vertexes = { m_begin_vertex };

	std::swap(m_begin_vertexes, m_end_vertexes);

	std::vector<std::vector<size_t>> new_vertexes(m_graph.size());
	std::vector<std::vector<char>> new_rules(m_rules.size());

	for (size_t i = 0; i < m_graph.size(); i++)
	{
		for (size_t j = 0; j < m_graph[i].size(); j++)
		{
			new_vertexes[m_graph[i][j]].push_back(i);
			new_rules[m_graph[i][j]].push_back(m_rules[i][j]);
		}
	}

	m_graph = std::move(new_vertexes);
	m_rules = std::move(new_rules);
}

bool StateMachine::Imitate(const std::string& input)
{
	assert(m_begin_vertexes.size() >= 1);
	size_t cur_state = *m_begin_vertexes.begin();

	for (const auto symbol : input)
	{
		auto iter = std::find(m_rules[cur_state].begin(), m_rules[cur_state].end(), symbol);
		
		if (iter == m_rules[cur_state].end())
			return false;

		cur_state = m_graph[cur_state][std::distance(m_rules[cur_state].begin(), iter)];
	}

	return m_end_vertexes.find(cur_state) != m_end_vertexes.end();
}

std::string createString(char sym)
{
	std::string res;
	res.push_back(sym);

	return res;
}

std::ostream& StateMachine::ToDot(std::ostream& ostr)
{
	ostr << "digraph StateMachine {\n";

	for (size_t i = 0; i < m_graph.size(); i++)
	{
		for (size_t j = 0; j < m_graph[i].size(); j++)
		{
			ostr << "\t" << i << " -> " << m_graph[i][j] << "[label=\"" << ((m_rules[i][j] == '\0') ? std::string("Eps") : createString(m_rules[i][j])) << "\"];\n";
		}
	}
	ostr << "}\n";

	return ostr;
}
