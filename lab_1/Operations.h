#pragma once

#include <unordered_map>


struct Operation
{
    int operands_count;
    int priority;
};

static std::unordered_map<char, Operation> operations = {
        {'(', {0, 0}},
        {')', {0, 0}},
        {'*', {1, 3}},
        {'+', {1, 3}},
        {'.', {2, 2}},
        {'|', {2, 1}}
};