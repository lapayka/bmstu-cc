#pragma once

#include <stack>

template<typename T>
class my_stack : public std::stack<T>
{
public:
    bool pop(T& elem)
    {
        if (std::stack<T>::empty())
            return false;

        elem = std::stack<T>::top();
        std::stack<T>::pop();
        return true;
    }
};