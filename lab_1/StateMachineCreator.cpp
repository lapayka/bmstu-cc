#include "StateMachineCreator.h"

#include <cassert>
#include <functional>
#include <fstream>

#include "Operations.h"
#include "MyStack.h"

StateMachine StateMachineCreator::CreateStateMachineByExpression(const std::string& reg_exp_polish)
{
	static std::unordered_map<char, std::function<StateMachine (const StateMachine &)>> one_operand_creators = 
	{
		{'*', CreateIterationStateMachine},
		{'+', CreatePositiveIterationStateMachine}
	};

	static std::unordered_map<char, std::function<StateMachine (const StateMachine&, const StateMachine&)>> two_operand_creators =
	{
		{'|', CreateUnionStateMachine},
		{'.', CreateConcatStateMachine}
	};

	my_stack<StateMachine> stack;

	for (const auto& elem : reg_exp_polish)
	{
		decltype(one_operand_creators)::iterator oiter;
		decltype(two_operand_creators)::iterator titer;
		if ((oiter = one_operand_creators.find(elem)) == one_operand_creators.end() &&
			(titer = two_operand_creators.find(elem)) == two_operand_creators.end()
			)
			stack.push(CreateOneSymbolStateMachine(elem));
		else
		{
			if (oiter != one_operand_creators.end())
			{
				StateMachine sm;
				stack.pop(sm);

				stack.push(oiter->second(sm));
			}
			else
			{
				StateMachine sm1, sm2;
				stack.pop(sm1);
				stack.pop(sm2);

				stack.push(titer->second(sm1, sm2));
			}
		}
	}

	StateMachine res;

	assert(stack.size() == 1);
	stack.pop(res);

	return res;
}

StateMachine StateMachineCreator::CreateIterationStateMachineTemplate()
{
	StateMachine res;

	res.m_graph = 
	{
		{1, 3},
		{},
		{1, 3},
		{}
	};
	res.m_rules =
	{
		{'\0', '\0'},
		{},
		{'\0', '\0'},
		{}
	};

	res.m_unfinished_vertexes =
	{
		{1, 2}
	};

	res.m_begin_vertex = 0;
	res.m_end_vertex = 3;

	return res;
}

StateMachine StateMachineCreator::CreatePositiveIterationStateMachineTemplate()
{
	StateMachine res;

	res.m_graph =
	{
		{1},
		{},
		{1, 3},
		{}
	};
	res.m_rules =
	{
		{'\0'},
		{},
		{'\0', '\0'},
		{}
	};

	res.m_unfinished_vertexes =
	{
		{1, 2}
	};

	res.m_begin_vertex = 0;
	res.m_end_vertex = 3;

	return res;
}

StateMachine StateMachineCreator::CreateUnionStateMachineTemplate()
{
	StateMachine res;

	res.m_graph =
	{
		{1, 3},
		{},
		{5},
		{},
		{5},
		{}
	};
	res.m_rules =
	{
		{'\0', '\0'},
		{},
		{'\0'},
		{},
		{'\0'},
		{}
	};

	res.m_unfinished_vertexes =
	{
		{1, 2},
		{3, 4}
	};

	res.m_begin_vertex = 0;
	res.m_end_vertex = 5;

	return res;
}

StateMachine StateMachineCreator::CreateConcatStateMachineTemplate()
{
	StateMachine res;

	res.m_graph =
	{
		{},
		{},
		{},
	};
	res.m_rules =
	{
		{},
		{},
		{}
	};

	res.m_unfinished_vertexes =
	{
		{0, 1},
		{1, 2}
	};

	res.m_begin_vertex = 0;
	res.m_end_vertex = 2;

	return res;
}

StateMachine StateMachineCreator::CreateOneSymbolStateMachine(char symbol)
{
	StateMachine res;

	res.m_graph =
	{
		{1},
		{}
	};
	res.m_rules =
	{
		{symbol},
		{}
	};

	res.m_begin_vertex = 0;
	res.m_end_vertex = 1;

	return res;
}

StateMachine StateMachineCreator::CreateIterationStateMachine(const StateMachine &sm)
{
	return CreateIterationStateMachineTemplate().InsertStateMachine(sm);
}

StateMachine StateMachineCreator::CreatePositiveIterationStateMachine(const StateMachine &sm)
{
	return CreatePositiveIterationStateMachineTemplate().InsertStateMachine(sm);
}

StateMachine StateMachineCreator::CreateConcatStateMachine(const StateMachine &lhs, const StateMachine &rhs)
{
	return CreateConcatStateMachineTemplate().InsertStateMachine(lhs).InsertStateMachine(rhs);
}

StateMachine StateMachineCreator::CreateUnionStateMachine(const StateMachine& lhs, const StateMachine &rhs)
{
	return CreateUnionStateMachineTemplate().InsertStateMachine(lhs).InsertStateMachine(rhs);
}
