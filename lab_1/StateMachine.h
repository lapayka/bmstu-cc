#pragma once

#include <vector>
#include <ostream>
#include <set>

class StateMachineCreator;

class StateMachine
{
	friend StateMachineCreator;
public:
	std::vector<std::vector<size_t>> m_graph;
	std::vector<std::vector<char>> m_rules;

	std::set<size_t> m_begin_vertexes;
	std::set<size_t> m_end_vertexes;

	using vertex = size_t;
	std::vector<std::pair<vertex, vertex>> m_unfinished_vertexes;

	vertex m_begin_vertex;
	vertex m_end_vertex;
public:
	StateMachine& InsertStateMachine(StateMachine rhs);

	void AddEndVertex(size_t idx) { m_end_vertexes.insert(idx); };
	void DeleteEps();
	void Determine();
	void Reverse();

	bool Imitate(const std::string& input);

	std::ostream& ToDot(std::ostream &ostr);
};

