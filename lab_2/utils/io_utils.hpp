#pragma once

#include <algorithm>
#include <iterator>
#include <ostream>

template <typename It>
void join(It begin, It end, std::ostream& os, const char* delimiter = ", ")
{
    if (begin != end) {
        std::copy(begin, std::prev(end), std::ostream_iterator<typename std::iterator_traits<It>::value_type>(os, delimiter));
        os << *std::prev(end);
    }
}


template <typename Container>
void join(const Container& container, std::ostream& os, const char* delimiter = ", ") 
{
    join(std::begin(container), std::end(container), os, delimiter);
}